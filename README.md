# CampaignsManager

This is App for managing campaigns.

## Running App

Install dependencies using `npm install`. <br/>
Run app using `npm run dev`.  
Backend was mocked with json-server.  
App runs json-server and frontend in the same time with concurrently library.  

## Functionalities

Add, Delete, Edit campaigns.

Sellers to enter the following information for each campaign:<br/>
• Campaign name<br/>
• Keywords<br/>
• Bid amount<br/>
• Campaign fund<br/>
• Status<br/>
• Town<br/>
• Radius<br/>

