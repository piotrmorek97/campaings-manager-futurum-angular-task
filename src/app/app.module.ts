import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CampaignsComponent } from './components/campaigns/campaigns.component';
import { CampaignItemComponent } from './components/campaign-item/campaign-item.component';
import { AddCampaignComponent } from './components/add-campaign/add-campaign.component';
import { AddCampaignBtnComponent } from './components/add-campaign-btn/add-campaign-btn.component';
import { EditCampaignComponent } from './components/edit-campaign/edit-campaign.component';
import { CampaignPanelComponent } from './components/campaign-panel/campaign-panel.component';
import { AboutComponent } from './components/about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CampaignsComponent,
    CampaignItemComponent,
    AddCampaignComponent,
    AddCampaignBtnComponent,
    EditCampaignComponent,
    CampaignPanelComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
