import { Component, OnInit } from '@angular/core';
import { CampaignValidator } from '../../utils/CampaignValidator';
import { CampaignService } from '../../services/campaign.service';

import { Campaign } from '../../models/Campaign';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-edit-campaign',
  templateUrl: './edit-campaign.component.html',
  styleUrls: ['./edit-campaign.component.scss']
})
export class EditCampaignComponent implements OnInit {
  visibility: boolean;
  campaign: Campaign;
  tempKeywordValue: string;

  constructor(private campaignService: CampaignService) { }

  ngOnInit(): void {
    const visibility: Observable<boolean> = this.getVisibility();
    this.setVisibility(visibility);

    this.campaignService.campaignItemToEdit
      .subscribe(campaignToEdit => {
        this.campaign = { ...campaignToEdit }
      });
  }

  closeModal(e): void {
    const editModal = document.getElementById('editModal');
    if(e.target == editModal) {
      this.campaignService.setEditModalVisibility(false);
    }
  }

  getKeywordsList(): string[] {
    return this.campaignService.keywordsListPrePopulated;
  }

  getTownsList(): string[] {
    return this.campaignService.townsListPrePopulated;
  }

  getVisibility(): Observable<boolean> {
    return this.campaignService.isEditModalVisible;
  }

  setVisibility(visibility: Observable<boolean>): void {
    visibility.subscribe(visibility => this.visibility = visibility);
  }

  changeTown(e): void {
    this.campaign.town = e.target.value;
  }

  changeKeyword(e): void {
    this.tempKeywordValue = e.target.value;
  }
  
  isNotKeywordAlreadyInArray(keyword: string): boolean {
    return this.campaign.keywords.indexOf(keyword) === -1
  }
  
  addKeyword(): void {
    if(this.tempKeywordValue && this.isNotKeywordAlreadyInArray(this.tempKeywordValue)) {
      this.campaign.keywords.push(this.tempKeywordValue);
    }
  }

  deleteKeyword(keyword: string): void {
    this.campaign.keywords = this.campaign.keywords.filter(k => k !== keyword);
  }

  onSubmit(): void {
    if(!CampaignValidator.hasSomeEmptyValues(this.campaign)) {
      this.campaignService.updateCampaign(this.campaign);
      this.campaignService.setEditModalVisibility(false);
      this.campaignService.setCampaignItemToEdit(null);
    }
  }
}
