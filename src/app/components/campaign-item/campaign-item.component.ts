import { Component, Input, NgModule, OnInit, EventEmitter, Output } from '@angular/core';
import { CampaignService } from '../../services/campaign.service';

import { Campaign } from '../../models/Campaign';

@Component({
  selector: 'app-campaign-item',
  templateUrl: './campaign-item.component.html',
  styleUrls: ['./campaign-item.component.scss']
})
export class CampaignItemComponent implements OnInit {
  @Input() campaign: Campaign;
  @Output() deleteCampaign: EventEmitter<Campaign> = new EventEmitter();

  constructor(private campaignService: CampaignService) { }

  ngOnInit(): void { }

  onDelete(campaign: Campaign) {
    console.log(campaign.id)
    this.deleteCampaign.emit(campaign);
  }

  onEdit(campaign: Campaign) {
    this.campaignService.setCampaignItemToEdit(campaign);
    this.campaignService.setEditModalVisibility(true);
  }

}
