import { Component, OnInit } from '@angular/core';
import { CampaignService } from '../../services/campaign.service';
import { CampaignValidator } from '../../utils/CampaignValidator';

import { Campaign } from '../../models/Campaign';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-campaign',
  templateUrl: './add-campaign.component.html',
  styleUrls: ['./add-campaign.component.scss']
})
export class AddCampaignComponent implements OnInit {
  campaign: Campaign;
  visibility: boolean;
  tempKeywordValue: string;

  constructor(private campaignService: CampaignService) {
  }

  ngOnInit(): void {
    const visibility: Observable<boolean> = this.getVisibility();
    this.setVisibility(visibility);

    this.campaign = new Campaign();
    this.campaign.keywords = [];
  }

  closeModal(e): void {
    const addModal = document.getElementById('addModal');
    console.log(e)
    if(e.target == addModal) {
      this.campaignService.setAddModalVisibility(false);
    }
  }

  getKeywordsList(): string[] {
    return this.campaignService.keywordsListPrePopulated;
  }

  getTownsList(): string[] {
    return this.campaignService.townsListPrePopulated;
  }

  changeTown(e) {
    this.campaign.town = e.target.value;
  }

  changeKeyword(e) {
    this.tempKeywordValue = e.target.value;
  }

  isNotKeywordAlreadyInArray(keyword: string): boolean {
    return this.campaign.keywords.indexOf(keyword) === -1
  }
  
  addKeyword(): void {
    if(this.tempKeywordValue && this.isNotKeywordAlreadyInArray(this.tempKeywordValue)) {
      this.campaign.keywords.push(this.tempKeywordValue);
    }
  }

  deleteKeyword(keyword: string): void {
    this.campaign.keywords = this.campaign.keywords.filter(k => k !== keyword);
  }

  getVisibility(): Observable<boolean> {
    return this.campaignService.isAddModalVisible;
  }

  setVisibility(visibility: Observable<boolean>): void {
    visibility.subscribe(visibility => this.visibility = visibility);
  }

  onSubmit() {
    if(!CampaignValidator.hasSomeEmptyValues(this.campaign)) {
      this.campaignService.addCampaign(this.campaign);
      this.campaignService.setAddModalVisibility(false);
      // Clearing out
      this.campaign = new Campaign();
    }
  }
}
