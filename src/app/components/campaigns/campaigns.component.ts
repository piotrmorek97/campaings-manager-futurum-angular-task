import { Component, OnInit } from '@angular/core';
import { CampaignService } from '../../services/campaign.service';

import { Campaign } from '../../models/Campaign';

@Component({
  selector: 'app-campaigns',
  templateUrl: './campaigns.component.html',
  styleUrls: ['./campaigns.component.scss']
})
export class CampaignsComponent implements OnInit {
  campaigns: Campaign[];
  loading: boolean;

  constructor(private campaignService: CampaignService) { } 

  ngOnInit(): void {
    this.campaignService.getCampaigns();
    this.campaignService.currentCampaigns
      .subscribe(campaigns => {
        this.campaigns = campaigns;
      })
  }

  deleteCampaign(campaign: Campaign) {
    this.campaignService.deleteCampaign(campaign);
  }

}
