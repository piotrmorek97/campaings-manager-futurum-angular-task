import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCampaignBtnComponent } from './add-campaign-btn.component';

describe('AddCampaignBtnComponent', () => {
  let component: AddCampaignBtnComponent;
  let fixture: ComponentFixture<AddCampaignBtnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddCampaignBtnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCampaignBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
