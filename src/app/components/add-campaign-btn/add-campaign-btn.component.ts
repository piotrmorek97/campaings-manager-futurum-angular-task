import { Component, OnInit } from '@angular/core';

import { CampaignService } from '../../services/campaign.service';

@Component({
  selector: 'app-add-campaign-btn',
  templateUrl: './add-campaign-btn.component.html',
  styleUrls: ['./add-campaign-btn.component.scss']
})
export class AddCampaignBtnComponent implements OnInit {

  constructor(private campaignService: CampaignService) { }

  ngOnInit(): void {
  }

  showAddCampaignModal() {
    this.campaignService.setAddModalVisibility(true);
  }

}
