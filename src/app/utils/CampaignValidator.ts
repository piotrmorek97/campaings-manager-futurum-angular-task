import { Campaign } from '../models/Campaign';

export class CampaignValidator {
  constructor() {}

  static hasSomeEmptyValues(campaign: Campaign): boolean {
    return Object.keys(campaign).some(k => campaign[k] === null || campaign[k] === '' || campaign[k] === undefined || campaign[k].length === 0);
  }
}