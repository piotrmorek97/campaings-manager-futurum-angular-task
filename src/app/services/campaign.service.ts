import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

import { Campaign } from '../models/Campaign';

const httpHeaderDetails = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class CampaignService {
  // Urls
  campaignsUrl: string = '/campaigns';

  // Prepopulated list
  keywordsListPrePopulated: string[] = ['medicine', 'hi-tech', 'advertisement', 'environment', 'education'];
  townsListPrePopulated: string[] = ['Warsaw', 'Cracow', 'Katowice', 'Gdansk', 'Sopot'];

  // Campaigns state
  private campaignsSource = new BehaviorSubject<Campaign[]>(null);
  currentCampaigns = this.campaignsSource.asObservable();

  // Modals visibility
  private isAddModalVisibleSource = new BehaviorSubject<boolean>(false);
  isAddModalVisible = this.isAddModalVisibleSource.asObservable();

  private isEditModalVisibleSource = new BehaviorSubject<boolean>(false);
  isEditModalVisible = this.isEditModalVisibleSource.asObservable();
  
  // Current CampaignItem to edit
  private campaignItemToEditSource = new BehaviorSubject<Campaign>(null);
  campaignItemToEdit = this.campaignItemToEditSource.asObservable();

  constructor(private http: HttpClient) { }

  getCampaigns(): void {
    this.http.get<Campaign[]>(this.campaignsUrl)
      .subscribe(campaigns => {
        console.log(campaigns);
        this.campaignsSource.next(campaigns);
      });
  }

  addCampaign(campaign: Campaign): void {
    this.http.post<Campaign>(this.campaignsUrl, campaign, httpHeaderDetails)
      .subscribe(campaign => {
        const currentCampaigns = this.campaignsSource.getValue();
        const updatedCampaigns = [...currentCampaigns, campaign];
        this.campaignsSource.next(updatedCampaigns);
      })
  }

  updateCampaign(campaign: Campaign): void {
    const url = `${this.campaignsUrl}/${campaign.id}`;
    this.http.put<Campaign>(url, campaign, httpHeaderDetails)
      .subscribe(campaign => {
        const currentCampaigns = this.campaignsSource.getValue();
        const updatedCampaigns = currentCampaigns.map(c => c.id === campaign.id ? campaign : c)
        this.campaignsSource.next(updatedCampaigns);
      })
  }

  deleteCampaign(campaign: Campaign): void {
    const url = `${this.campaignsUrl}/${campaign.id}`;
    this.http.delete<Campaign>(url)
      .subscribe(() => {
        const currentCampaigns = this.campaignsSource.getValue();
        const updatedCampaigns = currentCampaigns.filter(c => campaign.id !== c.id)
        this.campaignsSource.next(updatedCampaigns);
      })
  }

  setAddModalVisibility(visibility: boolean): void {
    this.isAddModalVisibleSource.next(visibility);
  }
  
  setEditModalVisibility(visibility: boolean): void {
    this.isEditModalVisibleSource.next(visibility);
  }

  setCampaignItemToEdit(campaign: Campaign): void {
    this.campaignItemToEditSource.next(campaign);
  }
}
